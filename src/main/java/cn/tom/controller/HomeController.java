package cn.tom.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.tom.bean.RequestMap;
import cn.tom.service.UserSerivce;

@Controller
public class HomeController extends OpenController{

	
	@Resource
	UserSerivce userService;
	
	@RequestMapping("/index")
	public String index(Model model, @RequestParam  Map<String,String> params, @RequestParam(name="id") int id) {
		System.out.println(params);
		System.out.println(id);
		model.addAttribute("host", "123123");
		return "index";
	}

	
	@RequestMapping("/hello")
	public Object hello(Model model) throws SQLException {
		Object obj =  userService.getU();
		userService.upU();
		model.addAttribute("host", obj);
		return "/account/user";
	}
}

package cn.tom.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;

import cn.tom.bean.ReqData;
import cn.tom.bean.RespData;
import cn.tom.dao.BaseDao;
import cn.tom.kit.StringUtil;
import cn.tom.kit.cache.TimedCache;
import cn.tom.listener.RequestContext;


public class BaseService extends BaseDao {
	
	@Resource
	protected TimedCache<String, Object> timedCache;
	
	/**
	 * 分页1
	 * @param sql
	 * @param _class
	 * @param reqData
	 * @param obj
	 * @return
	 */
	public <T> RespData<List<T>> page(String sql, Class<T> _class,
                                      ReqData reqData, Object... obj) {
		RespData<List<T>> respdata = new RespData<List<T>>();
		respdata.setPagenumber(reqData.getPage());
		// 总记录数
		int total = this.getInt("SELECT count(*) FROM (" + sql + ")c", obj);
		respdata.setNum(reqData, total);
		if (total == 0) {
			respdata.setRows(new ArrayList<T>());
			return respdata;
		}

		if (reqData.getSort() != null) {
			sql += " ORDER BY " + reqData.getSort() + " " + reqData.getOrder() == null ? ""
					: reqData.getOrder();
		}
		int start = (reqData.getPage() - 1) * reqData.getRows();
		List<T> rows = this.split(sql, _class, start, reqData.getRows(), obj);
		respdata.setRows(rows);

		return respdata;
	}

	/**
	 * 分页2
	 * 
	 * @param sql
	 * @param reqData
	 * @param obj
	 * @return
	 */
	public RespData<List<Map<String, Object>>> page(String sql,
			ReqData reqData, Object... obj) {
		RespData<List<Map<String, Object>>> respData = new RespData<List<Map<String, Object>>>();
		respData.setPagenumber(reqData.getPage());
		int cnt = super.getInt("SELECT COUNT(*) FROM (" + sql + ")CNT", obj);
		respData.setNum(reqData, cnt);
		if (cnt == 0) {
			respData.setRows(new ArrayList<Map<String, Object>>());
			return respData;
		}

		if (reqData.getSort() != null) {
			sql += " ORDER BY " + reqData.getSort() + " " + reqData.getOrder();
		}
		int start = (reqData.getPage() - 1) * reqData.getRows();
		respData.setRows(super.split(sql, start, reqData.getRows(), obj));

		return respData;
	}
	
	public RespData<List<Map<String, Object>>> page(String sql, String cntsql,
			ReqData reqData, Object... obj) {
		RespData<List<Map<String, Object>>> respData = new RespData<List<Map<String, Object>>>();
		if(reqData.getPage() < 1){
			reqData.setPage(1);
		}

		respData.setPagenumber(reqData.getPage());
		int cnt = super.getInt(cntsql);
		if (cnt == 0) {
			respData.setRows(new ArrayList<Map<String, Object>>());
			return respData;
		}

		int _ap = cnt%reqData.getRows() == 0 ? cnt/reqData.getRows():cnt/reqData.getRows()+1 ;
		if(_ap < reqData.getPage()){
			//respData.setPagenumber(_ap);
			reqData.setPage(_ap);
		}

		respData.setNum(reqData, cnt);

		if (reqData.getSort() != null) {
			sql += " ORDER BY " + reqData.getSort() + " " + reqData.getOrder();
		}
		int start = (respData.getPagenumber() - 1) * reqData.getRows();
		respData.setRows(super.split(sql, start, reqData.getRows(), obj));

		return respData;
	}
	
	public Map<String, Object> error(String msg, Object... obj) {
		HashMap<String, Object> error = new HashMap<>();
		error.put("msg", msg);
		error.put("state", "001");
		if (obj != null && obj.length > 0) {
			error.put("data", obj[0]);
		}
		return error;
	}

	public Map<String, Object> correct(String msg, Object... obj) {
		HashMap<String, Object> correct = new HashMap<>();
		correct.put("msg", msg);
		correct.put("state", "000");
		if (obj != null && obj.length > 0) {
			correct.put("data", obj[0]);
		}
		return correct;
	}
	
	public Map<String, Object> getSuser(){
		String token = RequestContext.get().getRequest().getHeader("token");
		Map<String,Object> user = (Map<String, Object>) timedCache.get(token);
		System.out.println(token +"==" + user);
		return user ;
	}
	
	public int getSuserId(){
		return (int) getSuser().get("id");
	}
	
	
	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -5);
		System.out.println(c.getTime());
	}

	public Integer intV(String target){
		if(StringUtil.hasLength(target)){
			return Integer.valueOf(target) ;
		}
		return null	;
	}

	public Double doubleV(String target){
		if(StringUtil.hasLength(target)){
			return Double.valueOf(target) ;
		}
		return null	;
	}


}

package cn.tom.util;

import cn.tom.kit.StringUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class BaseUtil {
	
	public static String getTodayStr(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(new Date());
	}

	public static String getThisMonth(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
		return df.format(new Date());
	}
	
	/**
	 * 获取下期时间   快乐8 9:05-23:55	| PK拾   9:07 - 23:57 	
	 * @param time
	 * @param minute
	 * @return
	 */
	public static Date addTimeMinute(Date time,int minute){
		Calendar ca = Calendar.getInstance();
		ca.setTime(time);
		ca.add(Calendar.MINUTE,minute);
		
		int h = ca.get(Calendar.HOUR_OF_DAY);
		int m = ca.get(Calendar.MINUTE);
		
		if((h>=23&&m>57)||h<9){
			ca.add(Calendar.HOUR_OF_DAY,9);
			ca.add(Calendar.MINUTE,5);
		}
		
		return ca.getTime();
	}
	

	/**
	 * yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @param formate
	 * @return
	 */
	public static Date parseToDate(String date, String formate){
		SimpleDateFormat df = new SimpleDateFormat(formate);
		Date d = null;
		try {
			d = df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}

	public static String getFormatDate() {
		return new java.sql.Date(System.currentTimeMillis()).toString().replace("-", "");
	}

	
	
	/**
	 * @param str
	 * @return
	 */
	public static String format(int str){
		return String.format("%03d", str);
	}


	public static Date getDate(String target) {
		if(StringUtil.hasLength(target)){
			return new Date() ;
		}
		int len = target.length();
		if(len == 10){
			return parseToDate(target,"yyyy-MM-dd") ;
		}
		if(len == 19){
			return parseToDate(target,"yyyy-MM-dd HH:mm:ss") ;
		}
		return new Date() ;
	}
}

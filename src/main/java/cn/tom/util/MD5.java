package cn.tom.util;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * @作者 yq
 * @创建日期  2010-11-5
 * @版本 V 1.0
 * @描述 MD5加密
 */
public class MD5 {
	private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

	/**
	 * 转换字节数组为16进制字串
	 * 
	 * @param b
	 *            字节数组
	 * @return 16进制字串
	 */

	public static String byteArrayToHexString(byte[] b) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			resultSb.append(byteToHexString(b[i]));
		}
		return resultSb.toString();
	}

	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}

	public static String MD5Encode(String origin) {
		String resultString = null;

		try {
			resultString = new String(origin);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byteArrayToHexString(md.digest(resultString
					.getBytes()));
		} catch (Exception ex) {

		}
		return resultString;
	}
	
	public static byte[] encryptMD5(byte[] data) throws Exception {  
		  
	    MessageDigest md5 = MessageDigest.getInstance("MD5");  
	    md5.update(data);  
	    
	    
	  
	    return md5.digest();  
	  
	}  

//	/4297f44b13955235245b2497399d7a93
	public static void main(String[] args) throws Exception {
		
		System.out.println(MD5Encode("469317895LCYE") );
		
		//78ecd887f8c1384b44205655223ea030
	
	
	}

}
package cn.tom.listener;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static Map<String, String> apps = new HashMap<String, String>();
	
	public static String getFilePath(){
		String upath =  apps.get("uploadPath"); // /data/static/ 
		File file = new File(upath);
		if(file.isAbsolute()){
			return apps.get("staticUrl");  //静态域名   fpath + path
		}
		return apps.get("staticUrl") + upath;   //
	}
	
}

package cn.tom.listener;

import java.sql.SQLException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

import com.alibaba.druid.pool.DruidDataSource;

import cn.tom.kit.cache.TimedCache;

@Configuration
public class MyWebConfig extends WebMvcConfigurerAdapter{

	@Resource
	BaseInterceptor baseInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(baseInterceptor);
		super.addInterceptors(registry);
	}
	
	
	@Bean(name = "timedCache")
	public TimedCache<String, Object> getTimeCache() {
		return new TimedCache<String, Object>(30 * 6000);
	}
	
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "datasource")
	public DataSource getDataSource() throws SQLException {
		DruidDataSource dataSource = new DruidDataSource();
		return  dataSource;  
	}
	
	
	@Bean
	@ConfigurationProperties(prefix = "velocity")
	public VelocityConfigurer getVelocityConfigurer() {
	    VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
//	    velocityConfigurer.setResourceLoaderPath("/templates/");
	    Properties velocityProperties = new Properties();
	    velocityProperties.put("output.encoding", "UTF-8");
	    velocityProperties.put("input.encoding", "UTF-8");
//	    velocityProperties.put("velocimacro.library", "views/common/macro.vm");
	    velocityProperties.put("velocimacro.library.autoreload", true);
	    velocityConfigurer.setVelocityProperties(velocityProperties);
	    return velocityConfigurer;
	}
	
	@Bean
	@ConfigurationProperties(prefix = "velocity")
	public VelocityViewResolver getVelocityViewResolver() {
		VelocityViewResolver resolver = new VelocityViewResolver();
	    return resolver;
	}
	
}

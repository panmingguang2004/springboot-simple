package cn.tom.bean;


/**
 * easy grid JSON
 */
public class RespData<E> {

	private E rows; //List<Map<String,Object>> list;
	private long total; 		//总记录数
	private long totalpage;	//总页数
	private int pagenumber; //当前页码
	private int pagerow; //页大小
	private int s; //数据从第几条开始显示
	private int e; //到第几条结束

	public E getRows() {
		return this.rows;
	}

	public void setRows(E  rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return this.total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
	
	public long getTotalpage() {
		return totalpage;
	}

	public void setTotalpage(long totalpage) {
		this.totalpage = totalpage;
	}

	public int getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(int pagenumber) {
		this.pagenumber = pagenumber;
	}

	public int getPagerow() {
		return pagerow;
	}

	public void setPagerow(int pagerow) {
		this.pagerow = pagerow;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getE() {
		return e;
	}

	public void setE(int e) {
		this.e = e;
	}
	
	public void setNum(ReqData reqData,int total){
		this.pagerow = reqData.getRows();
		this.pagenumber = reqData.getPage();
		if(total==0){
			this.total = 0;
			this.totalpage = 0;
			this.s = 0;
			this.e = 0;
		}else{
			this.total =total;
			this.totalpage = total%reqData.getRows()==0 ? total/reqData.getRows() : (total/reqData.getRows())+1 ;
			this.s = (reqData.getPage() - 1) * reqData.getRows() +1 ;
			int end = reqData.getPage()*reqData.getRows();
			this.e = end > total ? total : end ;
		}
	}
	

}

package cn.tom.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Muser {
	private int id;
	private long tel;
	private String uname;
	private String password;
	private int role;
	private int state;
	private Date ctime;
	private long token;
	private String rel;
	private List<Map<String, Object>> menu;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTel() {
		return tel;
	}

	public void setTel(long tel) {
		this.tel = tel;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	public List<Map<String, Object>> getMenu() {
		return menu;
	}

	public void setMenu(List<Map<String, Object>> menu) {
		this.menu = menu;
	}

}

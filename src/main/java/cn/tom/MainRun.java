package cn.tom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;


//@ServletComponentScan // 扫描servlet 注解
//@SpringBootApplication // 取消包括了下面两个
@EnableAutoConfiguration
@ComponentScan({"cn.tom.controller","cn.tom.service", "cn.tom.listener"})
public class MainRun {
	
	public static void main(String[] args) {
		SpringApplication.run(MainRun.class, args);
	}
	
	
	/**
	 * add web模板目录到classpath
	 * set CLASSPATH="%BASEDIR%"\conf;"%BASEDIR%"\web;"%REPO%"\*
	 */

}
